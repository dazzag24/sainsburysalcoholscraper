import json
import re

def load_data():
	with open('items.json') as data_file:
		data = json.load(data_file)
		return data

def print_data(data):
	widths = [0]*len(data[0])
	for row in data:
		for i,x in enumerate(row):
			if len(x) > widths[i]:
				widths[i]=len(x)
	widths = [w + 4 for w in widths]
	for row in data:
		print("".join(x.ljust(w) for x,w in zip(row, widths)))

def process_data(data):
	for d in data:
		try:
			new = dict(d)
			new["percentage"] = proc_percentage(new)
			new["volume"] = proc_volume(new)
			new["cost"] = proc_cost(new)
			new["units"] = calc_units(new)
			new["price_per_units"] = calc_price_per_units(new)
			yield new
		except PercentageException as e:
			pass
			# print("Error with: {0} because {1}".format(d["name"],e))
		except TypeError as e:
			pass

def proc_percentage(item):
	try:
		perc = re.search("(\d+(\.\d+)?)", item["percentage"]).group(1)
		return float(perc)
	except AttributeError as e:
		raise PercentageException("No alcohol percentage found")

class PercentageException(Exception):
	pass

def proc_volume(item):
	try:
		vol_str_match = re.search("((\d+x)?\d+(\.\d+)?[a-zA-Z]?[lL])",item["name"])
		if not vol_str_match:
			print(item["name"])
		else:
			vol_str = vol_str_match.group(1)
		num = 1
		if "x" in vol_str:
			match = re.search("^(\d+)[a-zA-Z]+", vol_str)
			num = int(match.group(1))
		match = re.search("(?P<vol>\d+(\.\d+)?)(?P<unit>[a-zA-Z]+)\.?$", vol_str)
		vol = float(match.group("vol"))
		units = match.group("unit")
		if units == "cl":
			vol = vol * 10
		elif units == "L":
			vol = vol * 1000
		return vol*num
	except Exception as e:
		pass

def proc_cost(item):
	perc = re.search("(\d+\.\d\d)", item["cost"]).group(1)
	return float(perc)

def calc_units(item):
	return item["volume"] * item["percentage"] / 1000.0

def calc_price_per_units(item):
	return int(item["cost"] / item["units"] * 1000)/1000.0

def extract_key_vals(data):
	return [[d["price_per_units"], d["name"],
			d["volume"], d["cost"], d["units"]] for d in data]

def to_str(data):
	return [[str(x) for x in row] for row in data]

data = load_data()
data = process_data(data)
data = sorted(data, key=lambda x: x["price_per_units"])
data = extract_key_vals(data)
data = to_str(data)
print_data(data[:20])
print()
print_data(data[-10:])


